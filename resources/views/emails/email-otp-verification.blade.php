<p>Hello {{$auth->name}}</p><br><br>
<p>
    Below is your one time password
</p><br><br>
<p>
{{$otp}}
</p>
<br>
<br>
<p>
    We’re here to help if you need it. Visit the ToDo <a style="color: #45b1f0;" href="#">Support</a> for more info or <a style="color: #45b1f0;" href="#">contact us</a><br><br>
    Keep it safe.<br><br>
    Cheers,<br>
    Team ToDo
</p>
