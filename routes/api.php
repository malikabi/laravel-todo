<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Http\Controllers\Api\Authentication\RegisterController;
use App\Http\Controllers\Api\Authentication\LoginController;
use App\Http\Controllers\Api\User\ToDoController;
use App\Http\Controllers\Api\User\ProfileController;

Route::group(['prefix' => 'auth'], function () {

    // Registration Process
    Route::post('registration', [RegisterController::class, 'registration']);
    Route::post('verify-registration', [RegisterController::class, 'verifyRegistration']);

    // Login Process
    Route::post('login', [LoginController::class, 'login']);
    // Password Forget Process
    Route::post('forget-password', [LoginController::class, 'forgetPassword']);
    Route::post('verify-reset-password-OTP', [LoginController::class, 'verifyResetPasswordOTP']);
    Route::post('set-new-password', [LoginController::class, 'setNewPassword']);
});

Route::group(['middleware' => ['appJwtAuth']], function () {

    // Profile Routes
    Route::group(['prefix' => 'profile'], function () {
        Route::post('update', [ProfileController::class, 'update']);

    });

    // Todo
    Route::group(['prefix' => 'todo'], function () {
        Route::get('listing', [ToDoController::class, 'listing']);
        Route::get('detail/{id}', [ToDoController::class, 'detail']);
        Route::post('store', [ToDoController::class, 'store']);
        Route::post('update', [ToDoController::class, 'update']);
        Route::get('delete/{id}', [ToDoController::class, 'delete']);
    });

    Route::post('logout', [LoginController::class, 'logout']);
});
