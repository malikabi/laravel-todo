<?php
// pagination
define('DEFAULT_PER_PAGE', 10);

// error codes
define('ERROR_400', 400);
define('ERROR_403', 403);
define('ERROR_401', 401);
define('ERROR_500', 500);
// success codes
define('SUCCESS_200', 200);

