<?php


if (!function_exists('generate_otp')) {

    /**
     * Description: The following method is used to generate a unique verification codw
     * @author Muhammad Abid - DS
     * @return integer
     */
    function generateOTP($model, $column)
    {
        // generate the random code aginst a table
        do {
            $otp = rand(1111, 9999);
        } while (DB::table($model->getTable())->where($column, $otp)->exists());

        return $otp;
    }
}
