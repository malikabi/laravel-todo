<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Todo\DeleteRequest;
use App\Http\Requests\Api\User\Todo\DetailRequest;
use App\Http\Requests\Api\User\Todo\StoreRequest;
use App\Http\Requests\Api\User\Todo\UpdateRequest;
use App\Models\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ToDoController extends Controller
{
    private $todo;
    public function __construct()
    {
        $this->todo = new Todo();
    }

    public function listing(Request $request)
    {
        try {
            DB::beginTransaction();
            $inputs = $request->all();
            $query = $this->todo->newQuery();
            $query->whereUserId(Auth::id());
            if(!empty($inputs['search'])){
                $query->where('title', 'LIKE', '%' .$inputs['search']. '%')
                ->orWhere('description', 'LIKE', '%' .$inputs['search']. '%');
            }

            return $this->successWithData('Success', $query->get());
        } catch (QueryException $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        }
    }

    public function detail(DetailRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $inputs = $request->all();
            $query = $this->todo->newQuery();
            $query->whereUserId(Auth::id())->whereId($id);

            return $this->successWithData('Success', $query->first());
        } catch (QueryException $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        }
    }

    public function delete(DeleteRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $inputs = $request->all();
            $query = $this->todo->newQuery();
            $query->whereUserId(Auth::id())->whereId($id);

            if($query->delete()){
                return $this->success('Success');
            }
            return $this->error('Something went wrong', ERROR_400);
        } catch (QueryException $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        }
    }

    public function store(StoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $inputs = $request->all();
            $inputs['user_id'] = Auth::id();
            $todo = $this->todo->newInstance();
            $todo->fill($inputs);
            if($todo->save()){

                DB::commit();
                return $this->success('Todo stored successfully');
            }
            return $this->error('Something went wrong', ERROR_400);
        } catch (QueryException $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        }
    }

    public function update(UpdateRequest $request)
    {
        try {
            DB::beginTransaction();
            $inputs = $request->all();
            $todo = $this->todo->newQuery()->whereId($inputs['id'])->first();
            $todo->fill($inputs);
            if($todo->save()){

                DB::commit();
                return $this->success(__('general.username_mail_sent'));
            }
            return $this->error(__('general.username_mail_sent'), ERROR_400);
        } catch (QueryException $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        }
    }

}
