<?php

namespace App\Http\Controllers\Api\Authentication;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Authentication\RegistrationRequest;
use App\Http\Requests\Api\Authentication\VerifyRegistrationRequest;
use App\Jobs\SendMailJob;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Hash;

class RegisterController extends Controller
{
    private $user;
    public function __construct()
    {
        $this->user = new User();
    }

    public function registration(RegistrationRequest $request)
    {
        try {
            DB::beginTransaction();
            $inputs = $request->all();
            $inputs['password'] = Hash::make($inputs['password']);
            $user = $this->user->newInstance();
            $user->fill($inputs);
            if($user->save()){
                // for now we are going to activate the user
                $user->email_verified_at = Carbon::now();
                $user->save();
                // $user->token = generateOTP($user, 'token');
                // $user->save();
                // $data = array(
                //     'subject' => env('MAIL_FROM_NAME') . ', Verification',
                //     'to' => $user->email,
                //     'view' => 'emails.email-otp-verification',
                //     'body' => [
                //         'otp' => $user->token,
                //         'auth' => Auth::user()
                //     ],
                //     'cc' => [],
                //     'attachments' => []
                // );
                // dispatch(new SendMailJob($data));

                DB::commit();
                return $this->success(__('general.username_mail_sent'));
            }
            return $this->error(__('general.username_mail_sent'), ERROR_400);
        } catch (QueryException $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        }
    }


    public function verifyRegistration(VerifyRegistrationRequest $request)
    {
        try {
            DB::beginTransaction();
            $user = $this->user->newQuery()->whereToken($request['code'])->first();
            $user->token = NULL;
            $user->email_verified_at = Carbon::now();
            if($user->save()){
                DB::commit();
                return $this->success('verified');
            }
            DB::rollBack();
            return $this->error('Something went wrong', ERROR_400);
        } catch (QueryException $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        }
    }



}
