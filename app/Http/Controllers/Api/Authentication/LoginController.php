<?php

/*
 * This file is part of the DevStudio package.
 *
 * (c) Muhammad Abid <abid@devstudio.us>
 *
 */

namespace App\Http\Controllers\Api\Authentication;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Authentication\ForgetPasswordRequest;
use App\Http\Requests\Api\Authentication\ForgetUsernameRequest;
use App\Http\Requests\Api\Authentication\LoginRequest;
use App\Http\Requests\Api\Authentication\ResendPasswordOTPRequest;
use App\Http\Requests\Api\Authentication\SetNewPasswordRequest;
use App\Http\Requests\Api\Authentication\VerifyEmailOTPRequest;
use App\Http\Requests\Api\Authentication\VerifyResetPasswordOTPRequest;
use App\Jobs\SendMailJob;
use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use \Tymon\JWTAuth\JWTAuth;


class LoginController extends Controller
{

    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;
    private $user;

    /**
     * @param \Tymon\JWTAuth\JWTAuth $jwt
     */
    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
        $this->user = new User();
        $this->passwordReset = new PasswordReset();
    }

    /**
     * User Login
     */
    public function login(LoginRequest $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = $this->jwt->attempt($credentials)) {
                return response()->json(['success' => false,'message' => 'Invalid Credentials'], 400);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false,'message' => 'could_not_create_token'], 500);
        }

        if(!Auth::User()->email_verified_at){
            return response()->json(['success' => false,'message' => 'You\'r account is Inactive'], 400);
        }

        // all good so return the token
        return response()->json([
            'success' => true,
            'user' => Auth::User(),
            'token'      => $token,
            'token_type'        => 'bearer'
        ], 200);
    }

    public function forgetPassword(ForgetPasswordRequest $request)
    {
        try {
            DB::beginTransaction();
            $inputs = $request->all();
            $user = $this->user->newQuery()->whereEmail($inputs['email'])->whereNotNull('email_verified_at')->first();

            if(!$user){
                return $this->error('You\'re account is not verified yet', ERROR_400);
            }

            $passwordReset = $this->passwordReset->newInstance();
            $passwordReset->email = $user->email;
            $passwordReset->token = generateOTP($passwordReset, 'token');
            $passwordReset->created_at = Carbon::now();
            $passwordReset->save();
            $data = array(
                'subject' => env('MAIL_FROM_NAME') . ', Forgot Password',
                'to' => $user->email,
                'view' => 'emails.email-otp-verification',
                'body' => [
                    'otp' => $passwordReset->token,
                    'auth' => Auth::user()
                ],
                'cc' => [],
                'attachments' => []
            );
            dispatch(new SendMailJob($data));

        } catch (QueryException $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        }
    }

    public function verifyResetPasswordOTP(VerifyResetPasswordOTPRequest $request)
    {
        try {
            return $this->success('verified');
        } catch (QueryException $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        }
    }

    public function setNewPassword(SetNewPasswordRequest $request)
    {
        try {
            DB::beginTransaction();
            $inputs = $request->all();
            $passwordReset = $this->passwordReset->newQuery()->whereCode($inputs['code'])->first();
            if($passwordReset){
                $user = $this->user->newQuery()->whereEmail($passwordReset->email)->first();
                $user->password = Hash::make($inputs['password']);
                $user->save();
                $passwordReset->delete();
                DB::commit();
                return $this->success(__('general.reset_password_successful'));
            }
            DB::rollBack();
            return $this->error('Something went wrong', ERROR_400);
        } catch (QueryException $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), ERROR_500);
        }
    }

    public function logout(Request $request)
    {
        try {
            $this->jwt->logout();
            return $this->success(__('general.logout_success'));
        } catch (QueryException $e) {
            return $this->error($e->getMessage(), ERROR_500);
        } catch (Exception $e) {
            return $this->error($e->getMessage(), ERROR_500);
        }
    }
}
