<?php

namespace App\Http\Requests\Api\Authentication;

use App\Http\Requests\BaseRequest;

class RegistrationRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:30|regex:/^[A-Za-z\s\.-]*$/',
            'email' => 'required|min:3|max:50|unique:users,email',
            'password' => 'required|min:6|max:30|regex:^\S*(?=\S{6,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*$^',
        ];
    }


    public function messages()
    {
        return [
            'password.regex' => 'Password must contain 1 uppercase, 1 lowercase, number and special character',
        ];
    }
}
