<?php

namespace App\Http\Requests\Api\Authentication;

use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;

class VerifyResetPasswordOTPRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|exists:password_resets,token',
        ];
    }

    public function messages()
    {
        return [
            'code.exists' => __('general.invalid_code')
        ];
    }
}
