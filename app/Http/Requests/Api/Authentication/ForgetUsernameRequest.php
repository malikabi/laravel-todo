<?php

namespace App\Http\Requests\Api\Authentication;

use App\Http\Requests\BaseRequest;

class ForgetUsernameRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|without_spaces|email:dns,rfc'
        ];
    }

    public function messages()
    {
        return [
            'username.email' => 'The email must be a valid email address.'
        ];
    }
}
