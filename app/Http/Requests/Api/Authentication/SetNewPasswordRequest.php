<?php

namespace App\Http\Requests\Api\Authentication;

use App\Http\Requests\BaseRequest;

class SetNewPasswordRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|exists:password_resets,token',
            'password' => 'required|min:6|max:30|confirmed:password_confirmation|regex:^\S*(?=\S{6,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*$^'
        ];
    }

    public function messages()
    {
        return [
            'password.regex' => 'Password must contain 1 uppercase, 1 lowercase, number and special character',
            'code.exists' => __('general.invalid_code')
        ];
    }
}
