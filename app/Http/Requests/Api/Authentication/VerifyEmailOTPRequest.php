<?php

namespace App\Http\Requests\Api\Authentication;

use App\Http\Requests\BaseRequest;

class VerifyEmailOTPRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'business_reference_id' => 'required|exists:users,business_reference_id',
            'code' => 'required|exists:users,verification_code,business_reference_id,' . request('business_reference_id')
        ];
    }

    public function messages()
    {
        return [
            'code.exists' => __('general.invalid_code')
        ];
    }
}
