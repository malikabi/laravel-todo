<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

class SendMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle()
    {
        $email      = $this->data['to'];
        $subject    = $this->data['subject'];
        Mail::send($this->data['view'], $this->data['body'], function ($message) use ($email, $subject) {
            $message->to($email)
                ->subject($subject)
                ->replyTo(env('MAIL_FROM_ADDRESS'))
                ->cc(!empty($this->data['cc']) ? $this->data['cc'] : []);
            if (!empty($this->data['attachments'])) {
                foreach ($this->data['attachments'] as $key => $attachment) {
                    $message->attach($attachment['path'], ['as' => $attachment['name']]);
                };
            }
        });
    }
}
